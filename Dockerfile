FROM eclipse-temurin:17.0.7_7-jre-jammy
ENV ARTIFACT_NAME=webhook-master-ktor-all.jar
COPY ./build/libs/$ARTIFACT_NAME /tmp/whm.jar
WORKDIR /tmp
ENTRYPOINT ["java","-jar","whm.jar"]