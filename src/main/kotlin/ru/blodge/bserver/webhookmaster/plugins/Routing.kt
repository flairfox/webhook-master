package ru.blodge.bserver.webhookmaster.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import ru.blodge.bserver.webhookmaster.webhooks.WebhookMaster
import ru.blodge.bserver.webhookmaster.webhooks.WebhookResult

fun Application.configureRouting() {
    routing {
        authenticate("token-auth") {
            post("/trigger") {
                val formParameters = call.receiveParameters()
                val target = formParameters["name"] ?: throw NotFoundException("\"name\" parameter is required")

                val result = WebhookMaster.trigger(target)
                if (result is WebhookResult.Success)
                    call.respond(HttpStatusCode.OK)
                else
                    call.respondText(status = HttpStatusCode.InternalServerError, text = result.message)
            }
        }
    }
}
