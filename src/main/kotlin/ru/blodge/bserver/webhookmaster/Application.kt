package ru.blodge.bserver.webhookmaster

import io.ktor.server.application.*
import io.ktor.server.netty.*
import org.yaml.snakeyaml.Yaml
import ru.blodge.bserver.webhookmaster.plugins.configureRouting
import ru.blodge.bserver.webhookmaster.plugins.configureSecurity
import ru.blodge.bserver.webhookmaster.webhooks.WebhookMaster
import java.io.File

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.module() {
    val webhooksConfigFile = environment.config.propertyOrNull("ktor.webhooks.configFile")?.getString()
    loadWebhooks(webhooksConfigFile)

    configureSecurity()
    configureRouting()
}

fun loadWebhooks(webhooksConfigFile: String?) {
    if (webhooksConfigFile != null) {
        val file = File(webhooksConfigFile)
        val yaml = Yaml()

        val config = yaml.load<Map<String, Any>>(file.inputStream())

        config.forEach { entry ->
            WebhookMaster.registerWebhook(entry.key, entry.value as Map<String, Any>)
        }
    }
}
