package ru.blodge.bserver.webhookmaster.webhooks

interface WebhookBuilder {

    fun build(parameters: Map<String, Any>): Webhook

}