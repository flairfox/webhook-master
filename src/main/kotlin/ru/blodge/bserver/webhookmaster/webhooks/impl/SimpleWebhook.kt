package ru.blodge.bserver.webhookmaster.webhooks.impl

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.plugins.*
import io.ktor.util.logging.*
import ru.blodge.bserver.webhookmaster.webhooks.Webhook
import ru.blodge.bserver.webhookmaster.webhooks.WebhookBuilder
import ru.blodge.bserver.webhookmaster.webhooks.WebhookResult

class SimpleWebhook(private val webhookUrl: String) : Webhook {

    private val log = KtorSimpleLogger("ru.blodge.bserver.webhookmaster.webhooks.impl.SimpleWebhook")

    private val client = HttpClient(CIO)

    override suspend fun trigger(): WebhookResult {
        val response = client.request(webhookUrl) {
            method = HttpMethod.Post
        }

        return if (response.status.isSuccess()) {
            val message = "Webhook successfully triggered!"
            log.info(message)
            WebhookResult.Success(message)
        } else {
            val message = "Can't trigger webhook. ${response.status} ${response.bodyAsText()}"
            log.error(message)
            WebhookResult.Failure(message)
        }
    }

    companion object : WebhookBuilder {

        override fun build(parameters: Map<String, Any>): Webhook {
            val url =
                parameters
                    .getOrElse("webhook-url") { throw NotFoundException("Field \"webhook-url\" is required!") }
                    .toString()

            return SimpleWebhook(url)
        }

    }

}