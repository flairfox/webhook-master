package ru.blodge.bserver.webhookmaster.webhooks

import io.ktor.server.plugins.*
import io.ktor.util.logging.*
import kotlin.collections.set

object WebhookMaster {

    private val log = KtorSimpleLogger("ru.blodge.bserver.webhookmaster.webhooks.WebhookMaster")

    private val map = mutableMapOf<String, Webhook>()

    fun registerWebhook(name: String, parameters: Map<String, Any>) {
        val webhook = WebhookFactory.buildWebhook(parameters)

        if (!map.containsKey(name)) {
            map[name] = webhook
            log.info("${webhook::class.simpleName} with name $name successfully registered")
        } else {
            log.warn("Webhook with name $name already registered! Skipping...")
        }
    }

    suspend fun trigger(name: String): WebhookResult {
        log.info("Triggering webhook with name $name")
        return try {
            map[name]?.trigger() ?: throw NotFoundException("No webhook registered for name $name")
        } catch (e: Throwable) {
            log.error(e.message, e)
            WebhookResult.Failure(e.message ?: e.toString())
        }
    }

}