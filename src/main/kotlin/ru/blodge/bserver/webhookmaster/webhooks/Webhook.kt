package ru.blodge.bserver.webhookmaster.webhooks

interface Webhook {

    suspend fun trigger(): WebhookResult

}