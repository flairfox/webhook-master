package ru.blodge.bserver.webhookmaster.webhooks

sealed class WebhookResult(val message: String) {

    class Success(successMessage: String) : WebhookResult(successMessage)
    class Failure(failureMessage: String) : WebhookResult(failureMessage)

}
