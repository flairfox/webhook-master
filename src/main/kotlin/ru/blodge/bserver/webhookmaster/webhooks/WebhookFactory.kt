package ru.blodge.bserver.webhookmaster.webhooks

import io.ktor.server.plugins.*
import ru.blodge.bserver.webhookmaster.webhooks.impl.SimpleWebhook

object WebhookFactory {

    fun buildWebhook(parameters: Map<String, Any>): Webhook {

        return when (val type = parameters["type"]?.toString()) {
            "simple", null -> SimpleWebhook.build(parameters)
            else -> throw NotFoundException("No webhook found for type $type")
        }
    }

}